import scala.util.Try
import java.io.{File, FileNotFoundException, FileWriter, IOException, PrintWriter}
import scala.annotation.tailrec
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.io.Source
object Desafio_BigInt {




  def isInt(aString: String): Boolean = Try(aString.toInt).isSuccess

  def calc_ordenados(semOrdenacaoCalclados: mutable.Buffer[Int]): Unit = {
    val file_name = new File("Output_Calculados_Ordenados.txt")
    val print_arq = new PrintWriter(new FileWriter(file_name))
    val listaOrdenada = semOrdenacaoCalclados.toSet.toList.sorted

    for (i <- listaOrdenada) {
      val nw: BigInt = recursive_factorial(i)
      val linha = i + "!: fatorial: " + nw + " Proximo primo:" + nextPrime(i.toInt)
      println(linha)// printa numeros calculados
      print_arq.write(linha + "\n")
    }
    print_arq.close()
  }


  def calcTime(inicial: Long): Unit = {
    var time = (System.currentTimeMillis() - inicial) 
    println("Tempo: "+time)
  }

  def nextPrime(value: Int): Int = {
    var res: Int = value
    if (!isPrime(res)) {
      for (i <- 1 to 10) {
        res = res + 1
        if (isPrime(res))
          return res
      }

    }
    return res

  }

  def isPrime(i: Int): Boolean = {
    if (i <= 1)
      false
    else if (i == 2)
      true
    else
      !(2 until i).exists(n => i % n == 0)
  }

  @tailrec
  def recursive_factorial(number: BigInt, result: BigInt = 1): BigInt = {
    if (number == 0)
      result
    else
      recursive_factorial(number - 1, result * number)
  }

  def delete(): Unit = {
    val myObj = new File("time.txt");
    val myObj1 = new File("Output_Nao_Calculados_Ordenados.txt");
    val myObj2 = new File("Output_Calculados_Ordenados.txt");

    myObj.delete()
    myObj1.delete()
    myObj2.delete()

  }

  def main(args: Array[String]): Unit = {
    delete()
    val time_inicial = System.currentTimeMillis()
    var semOrdenacaoNaoCalculados = new ListBuffer[Int]()
    var semOrdenacaoCalclados = new ListBuffer[Int]()
    try {
      val fSource = Source.fromFile("Input.txt")
      for (line <- fSource.getLines) {
        if (isInt(line)) {
          val res = Integer.parseInt(line)
          if (res > 0 ) {
            semOrdenacaoCalclados += res
          } else {
            semOrdenacaoNaoCalculados += res
          }
        }
      }
      fSource.close()
      calc_ordenados(semOrdenacaoCalclados)

    } catch {
      case e: FileNotFoundException => println("Exception: File missing")
        System.exit(1)
    }
    calcTime(time_inicial)
  }
  System.gc()
}
