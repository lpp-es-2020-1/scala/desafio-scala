# Desafio Fatoriais e Números Primos

## Motivação
A motivação do desafio e testar ao limite como a linguagem lida com entrada de uma grande quantidade de números levando ao extremo do calculo do fatorial.

Scala possui três formas de realizar o desafio uma delas e usando o calculo de fatorial:
- Int: Foi obtido uma faixa de calculo muito baixa, foi possível calcular ate fatorial 16.
- Long: Foi obtido uma faixa de calculo muito baixa também sendo possível calcular somente ate o fatorial 20.

- BigInt: Foi possível calcular fatoriais ate acima de mil em tempo de Int ou Long.


## Descrição
#### Entada: A entrada se dará por um arquivo "Input.txt" podendo conter letras números e outros caracteres.

- Passar todos os dados para lista ou array;
- Remover todos os os itens duplicados;
- Ordenar de forma crescente os valores;
- Calcular o fatorial para cada elemento, se possível;
- Se o valor não puder ser calculado escrever no arquivo de saída para casos sem sucesso, por exemplo (" 18 Excedeu a faixa de valores Int no calculo fatorial ");
- Caso o cálculo seja possível, escrever no arquivo de sucesso, por exemplo ("3!: fatorial: 6 primo mais proximo: 3"); 

#### Saída: dois arquivos, um contendo os cálculos no modo de sucesso e outro contendo os no modo de falha
#### Objetivo: Ao fim de cada execução, obter o tempo de execução e realizar uma media.  


  



## Instalação


*Obrigatório possuir Java JDK*
### Terminal

No terminal UBUNTU instale repositório da linguagem Scala
 
```bash
sudo apt-get install scala
```
Para compilar o código no terminal

- Para desafio usando Int
```bash
Scala Desafio_Int.scala
``` 
- Para desafio usando Long
```bash
Scala Desafio_Long.scala
``` 


- Para desafio usando BigInt
```bash
Scala Desafio_BigInt.scala
``` 




## Tempos de execução
Metodologia de medição

Cada código foi executado cinco vezes, captados seus tempos de execução pela função de calculo de tempo interna.


Tempo de execução para 1000 números:


| Código  | Tempo(ms)   |
| ------| -----:|
| Int    | 214  |
| Long   |  222 |
| BigInt |  384  |

Tempo de execução para 10000 números:


| Código | Tempo(ms)   |
| ------ | -----:|
| Int    | 316  |
| Long   | 315  |
| BigInt | 335  |

Tempo de execução para 100000 números:


| Código    | Tempo(ms)   |
| ----- | -----:|
| Int    | 444  |
| Long   | 403  |
| BigInt | 444  |

Tempo de execução para 1000000 números:


| Código    | Tempo(ms)   |
| -------|-----:|
| Int    | 698  |
| Long   |  939  |
| BigInt |  954  |

