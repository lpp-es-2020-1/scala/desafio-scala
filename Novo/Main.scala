import java.io.{File, FileWriter, PrintWriter}
import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer
import scala.io.Source
import scala.util.Try

class obj(str :String){
  import LoadList._
  def lista: List[Int] = loadlist(str)
}

object LoadList {
  def isInt(aString: String): Boolean = Try(aString.toInt).isSuccess

  def loadlist(str: String): List[Int] = {
    var semOrdenacaoCalclados = new ListBuffer[Int]()
    for (line <- Source.fromFile(str).getLines) {
      if (isInt(line)) {
        semOrdenacaoCalclados += Integer.parseInt(line)
      }
    }
    val ordena = semOrdenacaoCalclados.toSet.toList.sorted
    return ordena
  }
}


abstract class mt extends Thread{
  var list:List[Int]
  var i:Int

  def factorial(n: Int): Int = {
    @tailrec def factorialAcc(acc: Int, n: Int): Int =
    {
      if (n <= 1)
        acc
      else
        factorialAcc(n * acc, n - 1)
    }
    factorialAcc(1, n)
  }

  def nextPrime(value: Int): Int = {
    var res: Int = value
    if (!isPrime(res)) {
      for (i <- 1 to 10) {
        res = res + 1
        if (isPrime(res))
          return res
      }
    }
    return res

  }

  def isPrime(i: Int): Boolean = {
    if (i <= 1)
      false
    else if (i == 2)
      true
    else
      !(2 until i).exists(n => i % n == 0)
  }

  override def run(): Unit ={
    if(i==0){
      val file_name = new File("Calculados.txt")
      val print_arq = new PrintWriter(new FileWriter(file_name))
      for(j<-list) {
        if (j > 0 && j <= 16) {
          val fat=factorial(j)
          val line = j + "!: fatorial: " + fat + " Proximo primo:" + nextPrime(fat)
          println(line)
          print_arq.write(line + "\n")
        }
      }
      print_arq.close()
    }
    if(i==1){
      val file_name = new File("nCalculados.txt")
      val print_arq = new PrintWriter(new FileWriter(file_name))
      for(j<-list) {
        if (j < 0 || j > 16) {
          print_arq.write(j + " Excedeu a faixa de valores Int no calculo fatorial" + "\n")
        }
      }
      print_arq.close()
    }
  }
}

object Main {

  def main(args: Array[String]): Unit = {
    val proj=new obj("Input.txt")
    val th0 = new mt() {
      override var list: List[Int] = proj.lista
      override var i: Int = 0
    }
    val th = new mt() {
      override var list: List[Int] = proj.lista
      override var i: Int = 1
    }
    th0.start()
    th.start()

  }
}
